# ROV Control System Project

The objective of the ROV Control System project is to create a control system capable of maintaining a target linear velocity and orientation while completing missions with an underwater Remotely Operated Vehicle (ROV). The ROV includes an onboard IMU which can be used as input into the system's control feedback loop. The vehicle includes two thrusters per axis (XYZ) for a total of six thrusters. This project includes a software framework for controlling the ROV and communicating with its thrusters and peripherals.

## Mission Objectives

* Complete the Controller update function by implementing a control feedback loop
* Use the completed software to lift a load from the floor of a tank
* Measure the difference between the expected movement of the ROV against the actual movement

## Development Environment

The ROV runs on a Linux-based operating system. For the best experience, developing in a similar Linux-based environment is recommended. To simplify this process, a VirtualBox virtual machine will be provided by your instructor. This virtual machine is preconfigured with all the software and libraries required to develop the ROV Control System software.

### Virtual Machine Installation

1. [Download](https://www.virtualbox.org/wiki/Download_Old_Builds_5_2) and install VirtualBox build for your operating system. Your normal operating system is called the "host".
2. Start VirtualBox and select: `Machine -> Add...`
3. Navigate to the `vm` folder provided by your instructor and choose the `Ubuntu` vbox file.
4. Select the new virtual machine and click `Settings`.
5. Click `Network` and then choose the `Adapter 3` tab.
6. Ensure your wireless network adapter is selected in the `Name` dropdown.
7. Click `OK`.
4. Click `Start`. The virtual machine will launch.

### Default Credentials

The default username and password for the virtual machine can be found in the `README.txt` file located in the `vm` folder provided by your instructor. The default password for the Raspberry Pi is the same as the virtual machine.

**IMPORTANT! Change these passwords after logging in for the first time!**

### Virtual Machine Tools

The virtual machine comes configured with several useful development tools. These tools can be launched by clicking on their desktop icons.

![Desktop icons example](docs/desktop-icons-example.png)

#### Terminal

The terminal is used for executing command line instructions. Using it will be required when deploying and starting the ROV software. It can be launched by clicking the top desktop icon in the above reference image.

#### Visual Studio Code

[Visual Studio Code](https://code.visualstudio.com/) is an lightweight software development tool that can be used to develop the control loop software. It can be launched by clicking the bottom desktop icon in the above reference image. It is already configured to open the project by default.

### Networking

In order to communicate between the virtual machine and the Raspberry Pi onboard the ROV, you will need to know the [IP address](https://en.wikipedia.org/wiki/IP_address) of your virtual machine. This can be found by [opening a terminal](#markdown-header-terminal) and entering the following command:

```bash
ifconfig | grep 192.168.47.
```

You should see something that looks similar to this:

```bash
inet addr:192.168.47.103  Bcast:192.168.47.255  Mask:255.255.255.0
```

The number after `inet addr:` will be your IP address. In this case it is `192.168.47.103`.

### ROS Network Configuration

The following steps are required to configure [ROS](http://www.ros.org/) to use your specific network.

#### Virtual Machine

1. Open a [terminal](#markdown-header-terminal) in the virtual machine.
2. Open the `.bashrc` file for editing.

    ```
    nano ~/.bashrc
    ```

3. Scroll down to the bottom of the file.
4. Replace the IP address in the last two lines with the IP address of the virtual machine. See the [Networking](#markdown-header-networking) section for more details. Do **not** delete the port (`:11311`) in the last line.
5. Use `Ctrl+O` followed by `Enter` to save your changes.
6. Use `Ctrl+X` to exit.


#### Raspberry Pi

1. Open a [terminal](#markdown-header-terminal) in the virtual machine.
2. Log into the Raspberry Pi using SSH.

    ```
    ssh wsu@192.168.47.100
    ```

    Type `yes` if you are asked if you want to continue connecting and enter the [password](#markdown-header-default-credentials).
3. Open the `.bashrc` file for editing.

    ```
    nano ~/.bashrc
    ```

4. Scroll down to the bottom of the file.
5. Replace the IP address in the last line with the IP address of the virtual machine. Do **not** delete the port (`:11311`) in the last line.
6. Use `Ctrl+O` followed by `Enter` to save your changes.
7. Use `Ctrl+X` to exit.

#### Copying Code to the Raspberry Pi

Run the following command to update the controller script on the Raspberry Pi with the one developed in the virtual environment:

```bash
scp ~/ros/catkin_ws/src/rov/scripts/controller.py wsu@192.168.47.100:~/ros/catkin_ws/src/rov/scripts/
```

You will be prompted for a [password](#markdown-header-default-credentials). When you type the password, characters will not appear for security purposes. Hit enter once you've typed the password.

## Usage

The following keymap is used to control the ROV with the keyboard:

| Key | Action |
| --------| ---------- |
| W | Move forward |
| A | Move left |
| S | Move backward |
| D | Move right |
| Q | Move up |
| E | Move down |
| 6 | Yaw right |
| 4 | Yaw left |
| 5 | Pitch up |
| 8 | Pitch down |
| 9 | Roll clockwise |
| 7 | Roll counterclockwise |

This keymap works best with a keypad.

Thruster information is communicated on the `/rov/thrusters/` [ROS topic](http://wiki.ros.org/Topics).

### Conventions

The following axis conventions are observed:

* Positive Z direction is defined as forward
* Positive X direction is defined as right
* Positive Y direction is defined as up

The following unit conventions are observed:

* Translation movements are defined to be 5 cm/s while the corresponding key is pressed. 
* Rotational movements are defined to be &pi; / 32 rad/s while the corresponding key is pressed.

### Starting the System

To launch the system, first open a [terminal](#markdown-header-terminal) and start the [ROS master node](http://wiki.ros.org/roscore) by running the following:

```bash
roscore
```

Next start the publisher node which provides keyboard control input to the ROV. Do this by opening a second terminal and running the following:

```bash
sudo -i
source /home/wsu/.bashrc
rosrun rov publisher.py
```

You may be prompted for a [password](#markdown-header-default-credentials). When you type the password, characters will not appear for security purposes. Hit enter once you've typed the password.

Next log into the Raspbery Pi using SSH to start the onboard listener node:

```bash
ssh wsu@192.168.47.100
```

Be sure to use the correct IP address for your system and then enter the [password](#markdown-header-default-credentials).

Finally, start the listener node:

```bash
rosrun rov listener.py
```

You should now be able to drive the ROV using the controls described at the top of the [Usage](#markdown-header-usage) section.

## Third Party Libraries

This repository includes a [Unity](https://unity3d.com/) based simulator for testing purposes. This simulator relies on the excellent [ros-sharp](https://github.com/siemens/ros-sharp) package created by Dr. Martin Bischoff at Siemens AG which is licensed under the [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0) license.