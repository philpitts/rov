﻿using UnityEngine;

namespace RosSharp.RosBridgeClient
{
    public class ROVSubscriber : Subscriber<Messages.Geometry.Transform>
    {
        public Transform SubscribedTransform;

        private float previousRealTime;
        private Vector3 linearVelocity;
        private Quaternion rotation;
        private bool isMessageReceived;

        protected override void Start()
        {
            base.Start();
        }

        protected override void ReceiveMessage(Messages.Geometry.Transform message)
        {
            linearVelocity = ToVector3(message.translation).Ros2Unity();
            rotation = ToQuaternion(message.rotation);

            Debug.Log(rotation);

            isMessageReceived = true;
        }

        private static Vector3 ToVector3(Messages.Geometry.Vector3 geometryVector3)
        {
            return new Vector3(geometryVector3.x, geometryVector3.y, geometryVector3.z);
        }

        private Quaternion ToQuaternion(Messages.Geometry.Quaternion rotation)
        {
            return new Quaternion(
                rotation.x,
                rotation.y,
                rotation.z,
                rotation.w
            );
        }

        private void Update()
        {
            if (isMessageReceived)
                ProcessMessage();
        }
        private void ProcessMessage()
        {
            float deltaTime = Time.realtimeSinceStartup - previousRealTime;

            SubscribedTransform.Translate(linearVelocity * deltaTime);
            SubscribedTransform.rotation = rotation;

            previousRealTime = Time.realtimeSinceStartup;
            
            isMessageReceived = false;
        }

        
    }
}