#!/usr/bin/env python

import sys
import rospy

from controller import Controller
from pyquaternion import Quaternion
from geometry_msgs.msg import Transform

class Listener:

    def __init__(self):

        # Initialize controller
        self.controller = Controller()

        # Initialize and connect to ROS
        rospy.init_node('controller', anonymous=True)

        # Setup ROS communications
        subscribe_topic = "/rov/thrusters"
        rospy.Subscriber(subscribe_topic, Transform, self.callback)

    def callback(self, data):

        # Process control message payload
        velocity = (
            data.translation.x,
            data.translation.y,
            data.translation.z
        )
        rotation = Quaternion(
            x = data.rotation.x,
            y = data.rotation.y,
            z = data.rotation.z,
            w = data.rotation.w
        )

        self.controller.velocity = velocity
        self.controller.rotation = rotation

    def update(self):
        self.controller.update()

def main(args):
    listener = Listener()

if __name__ == '__main__':
    main(sys.argv)
