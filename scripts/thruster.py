#!/usr/bin/env python

import numpy
import RPi.GPIO as GPIO

class Thruster:

    def __init__(self, gpio1, gpio2):
        GPIO.setmode(GPIO.BOARD)
        frequency = 500 # Hz
        
        GPIO.setup(gpio1, GPIO.OUT)
        GPIO.setup(gpio2, GPIO.OUT)

        self.pwm1 = GPIO.PWM(gpio1, frequency)
        self.pwm2 = GPIO.PWM(gpio2, frequency)

        self.pwm1.start(0)
        self.pwm2.start(0)

    def set_power(self, power):
        power = numpy.clip(power, -100, 100)
        if power >= 0.0:
            # Forward
            self.pwm2.ChangeDutyCycle(0)
            self.pwm1.ChangeDutyCycle(power)
        else:
            # Backward
            self.pwm1.ChangeDutyCycle(0)
            self.pwm2.ChangeDutyCycle(-power)
