#!/usr/bin/env python

import sys
import time
import RPi.GPIO as GPIO

from imu import IMU
from thruster import Thruster
from pyquaternion import Quaternion
from geometry_msgs.msg import Transform

class Controller:

    def __init__(self):

        self.imu = IMU()
        self.thrusters = {
            'yaw_left' : Thruster(15, 16),
            'yaw_right' : Thruster(7, 8),
            'roll_left' : Thruster(12, 13),
            'roll_right' : Thruster(10, 11),
            'pitch_top' : Thruster(18, 19),
            'pitch_bottom' : Thruster(21, 22)
        }

        self.target_acceleration = (0.0, 0.0, 0.0)
        self.target_rotation = Quaternion()

        try:
            self.test_motors()
            while True:
                self.update()
        except:
            for key in self.thrusters:
                self.thrusters[key].set_power(0)
            GPIO.cleanup()
            sys.exit()

    def update(self):
        
        # TODO: Complete this method

        print('Target Rotation: {0}'.format(self.target_rotation.yaw_pitch_roll))
        print('Actual Rotation: {0}'.format(self.imu.orientation.yaw_pitch_roll))
        print('Target Acceleration: {0}'.format(self.target_acceleration))
        print('Actual Acceleration: {0}'.format(self.imu.linear_acceleration))

        # Examples (remove these)
        self.move_forward_and_backward()
        self.roll_over()
        self.speak()

        # Objective:
        # Make the target 'velocity' and 'rotation' parameters equal 
        # the actual values measured from the IMU

        # Positive X direction is defined as right
        # Positive Y direction is defined as up
        # Positive Z direction is defined as forward

        # Linear velocity vector order: xyz | Units: m/s

        # Control the thrusters with the Thruster::set_power() method.
        # Available thrusters are: x1, x2, y1, y2, z1, z2
        # Thrusters can be set from 0 (no power) to 100 (full power)
        # Example: self.thrusters['x1'].set_power(50)

        # Quaternions are useful
        # https://developerblog.myo.com/quaternions/

        # The included quaternion library has a few handy functions
        # http://kieranwynn.github.io/pyquaternion

        # If you want a rotation matrix use:
        # self.target_rotation.rotation_matrix AND
        # self.actual_rotation.rotation_matrix

    # ================================
    # Examples
    # ================================

    def move_forward_and_backward(self):
        z_motors = ['yaw_right', 'yaw_left', 'pitch_top', 'pitch_bottom']
                
        # Ramp up forward
        for x in range(100):
            for key in z_motors:
                self.thrusters[key].set_power(x)
                time.sleep(0.01)

        # Hold at full power forward for 5 seconds
        for key in z_motors:
            self.thrusters[key].set_power(100)
        time.sleep(5.0)

        # Ramp down forward
        for x in range(100):
            for key in z_motors:
                self.thrusters[key].set_power(100-x)
                time.sleep(0.01)
        
        # Ramp up backward
        for x in range(100):
            for key in z_motors:
                self.thrusters[key].set_power(-x)
                time.sleep(0.01)

        # Hold at full power backward for 5 seconds
        for key in z_motors:
            self.thrusters[key].set_power(-100)
        time.sleep(5.0)

        # Ramp down backward
        for x in range(100):
            for key in z_motors:
                self.thrusters[key].set_power(-100+x)
                time.sleep(0.01)

        for key in z_motors:
            self.thrusters[key].set_power(0)

    def roll_over(self):
        # Ramp up clockwise
        for x in range(100):
            self.thrusters['roll_left'].set_power(x)
            self.thrusters['roll_right'].set_power(-x)
            time.sleep(0.01)

        # Hold at full power forward for 5 seconds
        self.thrusters['roll_left'].set_power(100)
        self.thrusters['roll_right'].set_power(-100)
        time.sleep(5.0)

        # Ramp down clockwise
        for x in range(100):
            self.thrusters['roll_left'].set_power(100-x)
            self.thrusters['roll_right'].set_power(-100+x)
            time.sleep(0.01)
        
        # Ramp up backward
        for x in range(100):
            self.thrusters['roll_left'].set_power(-x)
            self.thrusters['roll_right'].set_power(x)
            time.sleep(0.01)

        # Hold at full power backward for 5 seconds
        self.thrusters['roll_left'].set_power(-100)
        self.thrusters['roll_right'].set_power(100)
        time.sleep(5.0)

        # Ramp down backward
        for x in range(100):
            self.thrusters['roll_left'].set_power(-100+x)
            self.thrusters['roll_right'].set_power(100-x)
            time.sleep(0.01)

        self.thrusters['roll_left'].set_power(0)
        self.thrusters['roll_right'].set_power(0)

    def speak(self):
        print('BARK!')

    def test_motors(self):
        for motor in self.thrusters.keys():
            # Ramp up forward
            for x in range(100):
                self.thrusters[motor].set_power(x)
                time.sleep(0.01)

            self.thrusters[motor].set_power(100)

            # Ramp down forward
            for x in range(100):
                self.thrusters[motor].set_power(100-x)
                time.sleep(0.01)

            self.thrusters[motor].set_power(0)
