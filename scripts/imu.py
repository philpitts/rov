#!/usr/bin/env python

import logging
import sys
import time

from Adafruit_BNO055 import BNO055
from pyquaternion import Quaternion

class IMU(object):

    def __init__(self):
        self.bno = BNO055.BNO055()
        if not self.bno.begin():
            raise RuntimeError('Failed to intialize BNO055')
        
        status, _self_test, error = self.bno.get_system_status()
        if status == 0x01:
            print('System error: {0}'.format(error))
            print('See BNO055 datasheet section 4.3.59 for error information')
        
    @property
    def orientation(self):
        """Orientation as a quaternion"""
        x,y,z,w = self.bno.read_quaternion()
        return Quaternion(x=x, y=y, z=z, w=w)

    @property
    def angular_velocity(self):
        """Angular velocity in degrees per second (xyz)"""
        return self.bno.read_gyroscope()

    @property
    def linear_acceleration(self):
        """Linear acceleration without gravity in m/s (xyz)"""
        return self.bno.read_linear_acceleration()