#!/usr/bin/env python

import sys
import rospy
import keyboard

import numpy as np

from pyquaternion import Quaternion
from geometry_msgs.msg import Transform
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import Quaternion as RosQuaternion

class Publisher:
    
    def __init__(self):

        # Setup ROS communications
        publish_topic = "/rov/thrusters"
        self.publisher = rospy.Publisher(publish_topic, Transform, queue_size = 10)
        
        #Initialize and connect to ROS
        rospy.init_node('controller', anonymous=True)

        # Set up member variables
        
        # Positive Z direction is defined as forward
        # Positive X direction is defined as right
        # Positive Y direction is defined as up
        
        self.angular_velocity = np.pi / 32 # PI / 32 rad/s
        self.linear_velocity = 0.05  # 5cm/s

        # Set initial orientation to identity
        self.orientation = Quaternion()

        # Publishing frequency
        self.rate = rospy.Rate(10) # 10Hz

        # Key bindings

        # Use WASD for translations
        # Use Numpad for rotations

        self.bindings = {
            'forward' : 'w',
            'left' : 'a',
            'back' : 's',
            'right' : 'd',
            'up' : 'q',
            'down' : 'e',
            'yaw right' : '6',
            'yaw left' : '4',
            'pitch up' : '5',
            'pitch down' : '8',
            'roll clockwise' : '9',
            'roll cclockwise' : '7'
        }

    def publish(self):

        # Process input
        self.update_orientation()
        translation = self.get_linear_velocities()

        # Convert the new linear velocity and orientation to ROS format
        transform = self.create_ros_transform(translation, self.orientation.elements)

        # Publish the new command and wait for the next cycle
        self.publisher.publish(transform)
        self.rate.sleep()

    def update_orientation(self):

        # Rotations are applied in yaw, roll, pitch order
        
        # Yaw
        direction = self.direction_from_keypress('yaw right', 'yaw left')
        self.apply_unit_rotation((0, 1, 0), direction)

        # Roll
        direction = self.direction_from_keypress('roll cclockwise', 'roll clockwise')
        self.apply_unit_rotation((0, 0, 1), direction)

        # Pitch
        direction = self.direction_from_keypress('pitch up', 'pitch down')
        self.apply_unit_rotation((1, 0, 0), direction)

    def get_linear_velocities(self):
        x = self.direction_from_keypress('right', 'left') * self.linear_velocity
        y = self.direction_from_keypress('up', 'down') * self.linear_velocity
        z = self.direction_from_keypress('forward', 'back') * self.linear_velocity
        return (x, y, z)

    def direction_from_keypress(self, positive, negative):
        result = 0
        if keyboard.is_pressed(self.bindings[positive]):
            result = 1
        elif keyboard.is_pressed(self.bindings[negative]):
            result = -1
        return result

    def apply_unit_rotation(self, axis, direction):
        if direction != 0:
            radians = self.angular_velocity * direction
            rotation = Quaternion(axis=axis, radians=radians)
            self.orientation = self.orientation * rotation

    def create_ros_transform(self, translation, rotation):
        ros_translation = Vector3(x=translation[0], y=translation[1], z=translation[2])
        ros_rotation = RosQuaternion(w=rotation[0], x=rotation[1], y=rotation[2], z=rotation[3])
        return Transform(ros_translation, ros_rotation)

def main(args):
    publisher = Publisher()
    while not rospy.is_shutdown():
        publisher.publish()

if __name__ == '__main__':
    main(sys.argv)